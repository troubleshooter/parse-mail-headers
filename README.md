# parse mail headers

Uses Email::Simple to extract the headers from a message.

I'm using it to examine spam. It parses all the headers, with a focus on the Received headers. It should be easy to alter it to examine any header you want. As it is currently written, it:

1.  finds all the Received headers
2.  finds the first Received header that was added to the mail (presumably the header added by the first MTA that received it)
3.  extracts the IP from that header
4.  does an rDNS lookup
5.  if there's a hostname, it looks up the nameservers for the base domain.

By "base domain" I mean that if the rDNS returns a hostname like "1234.my.example.domain.com", the base domain would be "domain.com".

The script is easily alterable if you need info for different headers. The base domain extraction from the hostname is less than optimal. I need to rewrite that portion of the code. I didn't have time to implement checks using Net::Domain::TLD. Hopefully, I'll get to this soon.

A couple of people have asked, "What's the purpose?" Well, I read a couple of articles about spammers using lots of different domains but having the same nameservers. So I wanted to check out some of the spam I have been getting to see if I could see any patterns.